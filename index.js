var deviceID = 'emulatorxxxxxxxxxxxxxxxxxxxxxxxx';

function getDateTime(timestamp) {
    https: //stackoverflow.com/questions/847185/convert-a-unix-timestamp-to-time-in-javascript
        var date = new Date(timestamp * 1000);
    var year = date.getFullYear();
    var month = date.getMonth();
    var day = date.getDate();
    var hours = date.getHours();
    var minutes = "0" + date.getMinutes();
    var seconds = "0" + date.getSeconds();
    return day + '/' + month + '/' + year + ' ' + hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
}

function getDeviceName() {
    const Http = new XMLHttpRequest();
    const url = 'http://localhost:5000/devices/' + deviceID + '/name';
    Http.open("GET", url, true);
    Http.send();

    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            data = JSON.parse(Http.responseText);
            document.getElementById("device").textContent = data.name;
        }
    }
}

function pollData() {
    // https://www.w3schools.com/js/js_json_http.asp
    const Http = new XMLHttpRequest();
    const url = 'http://localhost:5000/devices/' + deviceID + '/telemetry/latest';
    Http.open("GET", url, true);
    Http.send();

    Http.onreadystatechange = (e) => {
        if (Http.readyState == 4 && Http.status == 200) {
            data = JSON.parse(Http.responseText);
            document.getElementById("temp").textContent = data.data.temperature;
            document.getElementById("hum").textContent = data.data.humidity;
            document.getElementById("mst").textContent = data.data.moisture;
            document.getElementById("lux").textContent = data.data.lux;
            document.getElementById("timestamp").textContent = getDateTime(data.ts);
            console.log(data);
        }
    }
}

document.addEventListener("DOMContentLoaded", getDeviceName);
setInterval(pollData, 1000);